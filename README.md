# Mirror Monitor

Monitor the status of known mirrors of f-droid.org.

Deployed at https://fdroid.gitlab.io/mirror-monitor/.

## Active Mirrors

* https://f-droid.org/repo
* http://fdroidorg6cooksyluodepej4erfctzk7rrjpjbbr6wx24jh3lqyfwyd.onion/fdroid/repo
* https://cloudflare.f-droid.org/repo
* https://mirror.hyd.albony.in/fdroid/repo
* https://fdroid.tetaneutral.net/fdroid/repo
* https://mirror.cyberbits.eu/fdroid/repo
* https://bubu1.eu/fdroid/repo
* https://ftp.fau.de/fdroid/repo
* http://ftpfaudev4triw2vxiwzf4334e3mynz7osqgtozhbc77fixncqzbyoyd.onion/fdroid/repo
* https://plug-mirror.rcac.purdue.edu/fdroid/repo
* https://mirrors.tuna.tsinghua.edu.cn/fdroid/repo
* https://mirrors.nju.edu.cn/fdroid/repo
* https://mirror.iscas.ac.cn/fdroid/repo
* https://mirror.nyist.edu.cn/fdroid/repo
* https://mirrors.cqupt.edu.cn/fdroid/repo
* https://mirrors.shanghaitech.edu.cn/fdroid/repo
* https://mirrors.uestc.cn/fdroid/repo
* https://mirror.kumi.systems/fdroid/repo
* https://ftp.lysator.liu.se/pub/fdroid/repo
* http://lysator7eknrfl47rlyxvgeamrv7ucefgrrlhk7rouv3sna25asetwid.onion/pub/fdroid/repo
* https://mirror.librelabucm.org/fdroid/repo
* https://mirrors.dotsrc.org/fdroid/repo
* rsync://mirrors.dotsrc.org/fdroid/repo
* http://dotsrccccbidkzg7oc7oj4ugxrlfbt64qebyunxbrgqhxiwj3nl6vcad.onion/fdroid/repo
* https://mirror.ossplanet.net/fdroid/repo
* http://mirror.ossplanetnyou5xifr6liw5vhzwc2g2fmmlohza25wwgnnaw65ytfsad.onion/fdroid/repo
* https://ftp.gwdg.de/pub/android/fdroid/repo
* https://ftp.snt.utwente.nl/pub/software/fdroid/repo
* https://ftp.agdsn.de/fdroid/repo
* https://mirror.freedif.org/fdroid/repo
* rsync://mirror.freedif.org/fdroid/repo
* https://de.freedif.org/fdroid/repo
* rsync://de.freedif.org/fdroid/repo
* https://mirrors.jevincanders.net/fdroid/repo
* https://mirror.fcix.net/fdroid/repo
* rsync://mirror.fcix.net/fdroid/repo
* https://forksystems.mm.fcix.net/fdroid/repo
* rsync://forksystems.mm.fcix.net/fdroid/repo
* https://uvermont.mm.fcix.net/fdroid/repo
* rsync://uvermont.mm.fcix.net/fdroid/repo
* https://southfront.mm.fcix.net/fdroid/repo
* rsync://southfront.mm.fcix.net/fdroid/repo
* https://ziply.mm.fcix.net/fdroid/repo
* rsync://ziply.mm.fcix.net/fdroid/repo
* https://opencolo.mm.fcix.net/fdroid/repo
* rsync://opencolo.mm.fcix.net/fdroid/repo
* https://mirror01.komogoto.com/fdroid/repo
* rsync://mirror01.komogoto.com/fdroid/repo
* https://mirror.level66.network/fdroid/repo
* rsync://mirror.level66.network/fdroid/repo
* https://mirror.cyberbits.asia/fdroid/repo
* rsync://mirror.cyberbits.asia/fdroid/repo
* https://mirror.thore.io/fdroid/repo
* https://mirror.leitecastro.com/fdroid/repo
* rsync://mirror.leitecastro.com/fdroid/repo
* https://mirror.init7.net/fdroid/repo
* rsync://mirror.init7.net::fdroid-repo
* https://fdroid.rasp.sh/fdroid/repo
